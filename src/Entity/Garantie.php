<?php

namespace App\Entity;

use App\Repository\GarantieRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GarantieRepository::class)
 */
class Garantie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $typeGarantie;

    /**
     * @ORM\Column(type="boolean")
     */
    private $vol;

    /**
     * @ORM\Column(type="boolean")
     */
    private $panne;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $brisGlace;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $remplacement;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $petitRouleur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $franchise;

    /**
     * @ORM\OneToOne(targetEntity=Vehicule::class, inversedBy="garantie", cascade={"persist", "remove"})
     */
    private $vehicule;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTypeGarantie(): ?string
    {
        return $this->typeGarantie;
    }

    public function setTypeGarantie(string $typeGarantie): self
    {
        $this->typeGarantie = $typeGarantie;

        return $this;
    }

    public function getVol(): ?bool
    {
        return $this->vol;
    }

    public function setVol(bool $vol): self
    {
        $this->vol = $vol;

        return $this;
    }

    public function getPanne(): ?bool
    {
        return $this->panne;
    }

    public function setPanne(bool $panne): self
    {
        $this->panne = $panne;

        return $this;
    }

    public function getBrisGlace(): ?bool
    {
        return $this->brisGlace;
    }

    public function setBrisGlace(?bool $brisGlace): self
    {
        $this->brisGlace = $brisGlace;

        return $this;
    }

    public function getRemplacement(): ?bool
    {
        return $this->remplacement;
    }

    public function setRemplacement(?bool $remplacement): self
    {
        $this->remplacement = $remplacement;

        return $this;
    }

    public function getPetitRouleur(): ?bool
    {
        return $this->petitRouleur;
    }

    public function setPetitRouleur(?bool $petitRouleur): self
    {
        $this->petitRouleur = $petitRouleur;

        return $this;
    }

    public function getFranchise(): ?string
    {
        return $this->franchise;
    }

    public function setFranchise(string $franchise): self
    {
        $this->franchise = $franchise;

        return $this;
    }

    public function getVehicule(): ?Vehicule
    {
        return $this->vehicule;
    }

    public function setVehicule(?Vehicule $vehicule): self
    {
        $this->vehicule = $vehicule;

        return $this;
    }
}
