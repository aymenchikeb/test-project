<?php

namespace App\Entity;

use App\Repository\VehiculeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=VehiculeRepository::class)
 */
class Vehicule
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $marque;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $modele;

    /**
     * @ORM\Column(type="integer")
     */
    private $cheveaux;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $motorisation;

    /**
     * @ORM\Column(type="date")
     */
    private $dateCirculation;

    /**
     * @ORM\OneToOne(targetEntity=Garantie::class, mappedBy="vehicule", cascade={"persist", "remove"})
     */
    private $garantie;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getMarque(): ?string
    {
        return $this->marque;
    }

    public function setMarque(string $marque): self
    {
        $this->marque = $marque;

        return $this;
    }

    public function getModele(): ?string
    {
        return $this->modele;
    }

    public function setModele(string $modele): self
    {
        $this->modele = $modele;

        return $this;
    }

    public function getCheveaux(): ?int
    {
        return $this->cheveaux;
    }

    public function setCheveaux(int $cheveaux): self
    {
        $this->cheveaux = $cheveaux;

        return $this;
    }

    public function getMotorisation(): ?string
    {
        return $this->motorisation;
    }

    public function setMotorisation(string $motorisation): self
    {
        $this->motorisation = $motorisation;

        return $this;
    }

    public function getDateCirculation(): ?\DateTimeInterface
    {
        return $this->dateCirculation;
    }

    public function setDateCirculation(\DateTimeInterface $dateCirculation): self
    {
        $this->dateCirculation = $dateCirculation;

        return $this;
    }

    public function getGarantie(): ?Garantie
    {
        return $this->garantie;
    }

    public function setGarantie(?Garantie $garantie): self
    {
        // unset the owning side of the relation if necessary
        if ($garantie === null && $this->garantie !== null) {
            $this->garantie->setVehicule(null);
        }

        // set the owning side of the relation if necessary
        if ($garantie !== null && $garantie->getVehicule() !== $this) {
            $garantie->setVehicule($this);
        }

        $this->garantie = $garantie;

        return $this;
    }
}
