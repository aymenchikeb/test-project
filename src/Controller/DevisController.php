<?php

namespace App\Controller;

use App\Entity\Garantie;
use App\Entity\Proprietaire;
use App\Entity\Vehicule;
use App\Form\GarantieType;
use App\Form\ImmatriculeType;
use App\Form\PermisType;
use App\Form\PropType;
use App\Form\VehiculeType;
use SimpleXMLElement;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DevisController extends AbstractController
{
    #[Route('/', name: 'first_step_devis')]
    public function firstSTEP(Request $request): Response
    {
        $form = $this->createForm(PropType::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            
            $data = array(
                'nom'=>$form->get('Nom')->getData(),
                'prenom'=>$form->get('Prenom')->getData(),
                'dateNaissance'=>$form->get('dateNaissance')->getData(),
                'email'=>$form->get('email')->getData(),
                'telephone'=>$form->get('telephone')->getData(),
                'gender'=>$form->get('gender')->getData(),
            
            );

            return $this->redirectToRoute(
                'second_step_devis',
                $data,
                Response::HTTP_MOVED_PERMANENTLY // = 301
            );
        }

        return $this->render('Devis/index.html.twig',[
            'form'=>$form->createView()
        ]);
    }







    #[Route('/second_step', name: 'second_step_devis')]
    public function secondStep(Request $request): Response
    {
        if(
            $request->get('nom') && $request->get('prenom') && $request->get('dateNaissance') && $request->get('email') && $request->get('telephone') &&
            $request->get('gender')
        ){
          
            $form = $this->createForm(PermisType::class);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                
                $prop = new Proprietaire();
                $prop->setNom($request->get('nom'))
                    ->setPrenom($request->get('prenom'))
                    ->setDateNaissance(new \DateTime($request->get('dateNaissance')))
                    ->setEmail($request->get('email'))
                    ->setTelephone($request->get('telephone'))
                    ->setGender($request->get('gender'))
                    ->setPermisDate($form->get('permisDate')->getData());

                
                $manager = $this->getDoctrine()->getManager();

                $manager->persist($prop);

                $manager->flush();
                        

                return $this->redirectToRoute(
                    'third_step_devis',
                    array('year' => "year", 'month' => "month"),
                    Response::HTTP_MOVED_PERMANENTLY // = 301
                );
                
            }
            
        }else{
            return $this->redirectToRoute('first_step_devis');
        }

        return $this->render('Devis/permis.html.twig',[
            'form'=>$form->createView()
        ]);
    }




    #[Route('/third_step', name: 'third_step_devis')]
    public function thirdStep(Request $request): Response
    {
        $form = $this->createFormBuilder()
                    ->add('type',ChoiceType::class, array(
                        'choices'  => array(
                            'Votre voiture actuelle' => 'actuelle',
                            'Une nouvelle voiture' => 'nouvelle',
                            'Une voiture en cours de livraison' => 'en_cours',
                        ),
                        'multiple' => false,
                        'expanded' => true,
                        // 'choices_as_values' => true,
                        'required' => true
                    ))
                    ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            return $this->redirectToRoute(
                'fourth_step_devis',
                ['type'=>$form->get('type')->getData()],
                Response::HTTP_MOVED_PERMANENTLY // = 301
            );
        }

        return $this->render('Devis/vehiculeType.html.twig',[
            'form'=>$form->createView()
        ]);
    }

    #[Route('/fourth_step', name: 'fourth_step_devis')]
    public function fourthStep(Request $request): Response
    {
        
        if($request->get('type')){

            $formChercher = $this->createForm(ImmatriculeType::class);
    
            $formChercher->handleRequest($request);
    
    
            $formAjouterManuel = $this->createForm(VehiculeType::class);
    
            $formAjouterManuel->handleRequest($request);
    
            if($formChercher->isSubmitted() && $formChercher->isValid()){
                    
                    // $arrContextOptions=array(
                    //     "ssl"=>array(
                    //         "verify_peer"=>false,
                    //         "verify_peer_name"=>false,
                    //     ),
                    // );  
                    // $response = file_get_contents('https://www.regcheck.org.uk/api/reg.asmx/CheckFrance?RegistrationNumber=DB442VX&username=Backup',false, stream_context_create($arrContextOptions));
    
                    // $response = new SimpleXMLElement($response);
    
                    // dd($response);
    
                    // foreach($response[0] as $data) {
                    //     $id = $data->Description;
                    //     dd($id);
                    // }
                    // exit;
    
            }
            if($formAjouterManuel->isSubmitted() && $formAjouterManuel->isValid()){
                 
                $vericule = new Vehicule();
                $vericule->setMatricule($formAjouterManuel->get('matricule')->getData())
                     ->setMarque($formAjouterManuel->get('marque')->getData())
                     ->setModele($formAjouterManuel->get('modele')->getData())
                     ->setDateCirculation($formAjouterManuel->get('dateCirculation')->getData())
                     ->setCheveaux($formAjouterManuel->get('cheveaux')->getData())
                     ->setMotorisation($formAjouterManuel->get('motorisation')->getData());
    
                
                $manager = $this->getDoctrine()->getManager();
    
                $manager->persist($vericule);
    
                $manager->flush();
                        
    
                return $this->redirectToRoute(
                    'fifth_step_devis',
                    [
                        'vehicule_id'=>$vericule->getId(),
                        'type'=>$request->get('type')
                    ],
                    Response::HTTP_MOVED_PERMANENTLY // = 301
                );
            }
        }else{
            return $this->redirectToRoute('third_step_devis');
        }
        return $this->render('Devis/immatriculation.html.twig',[
            'formChercher'=>$formChercher->createView(),
            'formAjouterManuel'=>$formAjouterManuel->createView()
        ]);
    }
    
    
    
    
    #[Route('/fifth_step', name: 'fifth_step_devis')]
    public function fifthtep(Request $request): Response
    {
        $vehicule_id = $request->get('vehicule_id');
        if($vehicule_id && $request->get('type')){

            $vol = $request->get('vol') ? "true" : "false";
            $panne = $request->get('panne') ? "true" : "false";
            $brisGlace = $request->get('brisGlace') ? "true" : "false";
            $remplacement = $request->get('remplacement') ? "true" : "false";
            $petit_rouleur = $request->get('petit_rouleur') ? "true" : "false";
    
            $form = $this->createForm(GarantieType::class);
        
            $form->handleRequest($request);
    
    
            if($form->isSubmitted() && $form->isValid()){
                
                $voiture = $this->getDoctrine()->getRepository(Vehicule::class)->find($vehicule_id);
                
                $garantie = new Garantie();
                $garantie->setType($request->get('type'))
                         ->setTypeGarantie($form->get('typeGarantie')->getData())
                         ->setVol($vol)
                         ->setPanne($panne)
                         ->setBrisGlace($brisGlace)
                         ->setRemplacement($remplacement)
                         ->setPetitRouleur($petit_rouleur)
                         ->setFranchise($form->get('franchise')->getData())
                         ->setVehicule($voiture);
                
                $manager = $this->getDoctrine()->getManager();
    
                $manager->persist($garantie);
    
                $manager->flush();
                        
    
                return $this->redirectToRoute(
                    'last_step_devis',
                    [],
                    Response::HTTP_MOVED_PERMANENTLY // = 301
                );
                
                
            }

        
        }else{

            return $this->redirectToRoute('fourth_step_devis');

        }


        return $this->render('Devis/garantie.html.twig',[
            'form'=>$form->createView()
        ]);
    }


    #[Route('/last_step', name: 'last_step_devis')]
    public function laststep(Request $request): Response
    {
        return $this->render('Devis/thanks.html.twig',[
        ]);
    }
}


