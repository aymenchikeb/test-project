<?php

namespace App\Form;

use App\Entity\Proprietaire;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class PropType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('gender',ChoiceType::class, [
                'choices'  => [
                    'Madame' => true,
                    'Monsieur' => false,
                ],
            ])
            ->add('Nom',TextType::class,[
                    'constraints' => [
                    new NotNull([
                        'message'=>'Nom ne doit pas etre null'
                    ]),
                ]
            ])
            ->add('Prenom',TextType::class,[
                'constraints' => [
                    new NotNull([
                        'message'=>'Prenom ne doit pas etre null'
                    ]),
                ]
            ])
            ->add('dateNaissance',DateType::class,[
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'data' => new \DateTime(),
                'attr' => array('class' => 'form-control', 'style' => 'line-height: 20px;'),
            ])
            ->add('email',EmailType::class,[
                'constraints' => [
                    new NotNull([
                        'message'=>'Prenom ne doit pas etre null'
                    ]),
                    new Email([
                        'message'=>"L'email {{ value }} n'est pas valid."
                    ])
                ]
            ])
            ->add('telephone',IntegerType::class,[
                'constraints' => [
                    new NotNull([
                        'message'=>'Telephone ne doit pas etre null'
                    ]),
                    new Length([
                        'min'=>10,
                        'max'=>14,
                        'minMessage' => "Le numero de votre telephone doit depasser 10 numeros",
                        'maxMessage' => "Le numero de votre telephone ne doit pas depasser 14 numeros"
                    ])
                ]
            ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Proprietaire::class,
        ]);
    }
}
