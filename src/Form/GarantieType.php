<?php

namespace App\Form;

use App\Entity\Garantie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class GarantieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('typeGarantie',ChoiceType::class, [
                'choices'  => [
                    'Tout risque' => 'Tout risque',
                    'Tiers' => 'Tiers',
                    'Tiers simple' => 'Tiers simple',
                ],
            ])
            // ->add('type',TextType::class,[
            //     'constraints' => [
            //     new NotNull([
            //         'message'=>'Nom ne doit pas etre null'
            //     ]),
            //     ]
            // ])
            ->add('franchise',ChoiceType::class, [
                'choices'  => [
                    '0€' => '0€',
                    '100€' => '100€',
                    '200€' => '200€',
                    '300€' => '300€',
                    '400€' => '400€',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Garantie::class,
        ]);
    }
}
