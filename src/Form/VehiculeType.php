<?php

namespace App\Form;

use App\Entity\Vehicule;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotNull;

class VehiculeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('matricule',TextType::class,[
                'constraints' => [
                    new NotNull([
                        'message'=>'Matricule de cheveaux ne doit pas etre null'
                    ]),
                ]
            ])
            ->add('marque',TextType::class,[
                'constraints' => [
                    new NotNull([
                        'message'=>'Marque ne doit pas etre null'
                    ]),
                ]
            ])
            ->add('modele',TextType::class,[
                'constraints' => [
                    new NotNull([
                        'message'=>'Modele ne doit pas etre null'
                    ]),
                ]
            ])
            ->add('cheveaux',IntegerType::class,[
                'constraints' => [
                    new NotNull([
                        'message'=>'Nombre de cheveaux ne doit pas etre null'
                    ]),
                ]
            ])
            ->add('motorisation',TextType::class,[
                'constraints' => [
                    new NotNull([
                        'message'=>'Motorisation ne doit pas etre null'
                    ]),
                ]
            ])
            ->add('dateCirculation',DateType::class,[
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'data' => new \DateTime(),
                'attr' => array('class' => 'form-control', 'style' => 'line-height: 20px;'),
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Vehicule::class,
        ]);
    }
}
