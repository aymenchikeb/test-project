<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210714154357 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE garantie (id INT AUTO_INCREMENT NOT NULL, type VARCHAR(255) NOT NULL, type_garantie VARCHAR(255) NOT NULL, vol TINYINT(1) NOT NULL, panne TINYINT(1) NOT NULL, bris_glace TINYINT(1) DEFAULT NULL, remplacement TINYINT(1) DEFAULT NULL, petit_rouleur TINYINT(1) DEFAULT NULL, franchise VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE garantie');
    }
}
