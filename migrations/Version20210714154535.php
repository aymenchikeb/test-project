<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210714154535 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE garantie ADD vehicule_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE garantie ADD CONSTRAINT FK_7193C6284A4A3511 FOREIGN KEY (vehicule_id) REFERENCES vehicule (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_7193C6284A4A3511 ON garantie (vehicule_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE garantie DROP FOREIGN KEY FK_7193C6284A4A3511');
        $this->addSql('DROP INDEX UNIQ_7193C6284A4A3511 ON garantie');
        $this->addSql('ALTER TABLE garantie DROP vehicule_id');
    }
}
